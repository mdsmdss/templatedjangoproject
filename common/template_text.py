# -*- coding:utf-8 -*-
from django.conf import settings
import datetime

"""
SYSTEM ERROR
"""
NOT_CONNECTION          = 101 #通信エラー
NOT_JSON                = 102 #JSONエラー
NOT_PARAMETER                = 103 #パラメータがない
DB_ERROR                = 104 #DBエラー

"""
LOGIN ERROR
"""
NOT_REGISTPLAYER          = 201 #未登録ユーザー

"""
REGIST PLAYER ERROR
"""
REGIST_OVERLAP_PLAYER     = 202 #重複


"""
PLAYER INFO
"""
NOT_FOUND_PLAYER_INFO	= 301


"""
RESULT DATA FROM DB
"""
NO_RESULT_DATA			= 401


RESULT_FLG_SUCCESS = '0' # 成功

"""
初期無料キャラー
"""
GAME_START_GET_UNIT_ID_1 = 1 
GAME_START_GET_UNIT_ID_2 = 2 


