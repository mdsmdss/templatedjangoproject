# -*- coding:utf-8 -*-
from django.conf import settings

from moon.models import Unit
from common import template_text as T
import datetime

def genResponse(*args, **kwargs):
    result = []
    exception_keys = []
    if kwargs.has_key('exception_keys'):
        exception_keys = kwargs['exception_keys']
        
    exception_keys.append('_state')
    
    jsonDic = {}
    for record in args: 
        for row in record:
            temp = {}
            kyes = row.__dict__.keys()
            for key in kyes:
                if key in exception_keys:
                    continue
                if type(row.__dict__[key]) == unicode:
                    temp[key] = str(row.__dict__[key])
                elif type(row.__dict__[key]) == long:
                    temp[key] = int(row.__dict__[key])
                elif type(row.__dict__[key]) == datetime.datetime:
                    temp[key] = str(row.__dict__[key])
                else:
                    temp[key] = row.__dict__[key]
    
            result.append( temp )
                
    if result:
        jsonDic["field"] = result
        jsonDic["result"] = T.RESULT_FLG_SUCCESS;
    else:
        jsonDic["result"] = T.NO_RESULT_DATA
        
    return jsonDic
    
    
#def genResponse(record, exception_keys=list()):
#    result = []
#    exception_keys.append('_state')
#    jsonDic = {}
#    if record:
#        for row in record:
#            temp = {}
#            kyes = row.__dict__.keys()
#            for key in kyes:
#                if key in exception_keys:
#                	continue
#    
#                if type(row.__dict__[key]) == unicode:
#                    temp[key] = str(row.__dict__[key])
#                elif type(row.__dict__[key]) == long:
#                    temp[key] = int(row.__dict__[key])
#                elif type(row.__dict__[key]) == datetime.datetime:
#                    temp[key] = str(row.__dict__[key])
#                else:
#                    temp[key] = row.__dict__[key]
#    
#            result.append( temp )
#            
#        jsonDic["field"] = result
#        jsonDic["result"] = T.RESULT_FLG_SUCCESS;
#        
#    else:
#        jsonDic["result"] = T.NO_RESULT_DATA
#
#    return jsonDic

def get_unit(unit_id):
    """
    get unit
    """
    try:
        return Unit.objects.get(unit_id=unit_id);
    except Unit.DoesNotExist:
        return None 

    