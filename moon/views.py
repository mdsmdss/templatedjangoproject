# Create your views here.
# -*- coding: utf-8 -*-
from __future__ import absolute_import
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest, HttpResponseNotFound
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core import serializers
import logging
import json
from django.utils import simplejson
from moon.models import Unit
from common import momo_api
from common import template_text as T

def unit_master_data(request):
    """
    ユニットマスターデータ
    """
    uuid = request.POST.get('terminal_id',None)   
    jsonDic = {}
    result = None
   
    try:
        #結果がある場合jsonで返す
        
        result = Unit.objects.all()
    
        if result:
            jsonDic["field"] = momo_api.genResponse( result )
            jsonDic["result"] = T.RESULT_FLG_SUCCESS;
        else:
            jsonDic["result"] = T.NO_RESULT_DATA
    except Unit.DoesNotExist:
        jsonDic["result"] = T.DB_ERROR
   
    return HttpResponse(json.dumps(jsonDic))
