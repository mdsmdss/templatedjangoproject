from django.contrib import admin
from moon.models import Unit

class UnitAdmin(admin.ModelAdmin):
    #list_per_page = 100
    list_display=(
        'id',
        'name',
        'max_level',
        'type',
        'mineral',
        'hp',
        'attack_power',
        'attack_speed',
        'attack_range',
        'splash_range',
        'critical_hit',
        'move_speed',
        'rare',
        'cool_time',
        'falldown_distance',
        'drop_mineral',
        'detail',
    )
admin.site.register(Unit,UnitAdmin)

