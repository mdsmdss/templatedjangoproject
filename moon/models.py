# -*- coding: utf-8 -*-
from django.db import models
from django.utils.safestring import mark_safe

# Create your models here.

class Unit(models.Model):
    '''
    キャラクター　マスタデータ
    '''
    id = models.PositiveIntegerField(primary_key=True)
    unit_id = models.IntegerField(u'unit_id', default=0)
    name = models.CharField(u'名前', max_length=16)
    max_level = models.IntegerField(u'MAXレベル', default=0)
    type = models.IntegerField(u'タイプ', default=0)
    mineral = models.IntegerField(u'ミネラル', default=0)
    hp = models.IntegerField(u'体力', default=0)
    attack_power = models.IntegerField(u'攻撃力', default=0)
    attack_speed = models.FloatField(u'攻撃速度', default=0)
    attack_range = models.FloatField(u'攻撃範囲', default=0)
    splash_range = models.IntegerField(u'範囲', default=0)
    critical_hit = models.IntegerField(u'critical_hit', default=0)
    move_speed  = models.FloatField(u'移動速度', default=0)
    rare = models.IntegerField(u'レア度', default=0)
    cool_time  = models.FloatField(u'レンジャー出発時間', default=0)
    falldown_distance  = models.FloatField(u'倒れた距離', default=0)
    drop_mineral = models.IntegerField(u'drop_mineral', default=0)
    detail = models.CharField(u'名前レンジャーの説明', max_length=200)